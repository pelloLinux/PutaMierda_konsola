
public class Karta {

	private int numero;
	private int palo;
	private int baliotasuna;

	//1=urrie
	//2=kopa
	//3=espada
	//4=basto

	public Karta(int palo, int numero, int balio){

		this.setNumero(numero);
		this.setPalo(palo);
		this.setBaliotasuna(balio);

	}
	public void inprimatu()
	{
		if(this.getPalo()==1)
			System.out.print("Urrea ::: ");
		else if(this.getPalo()==2)
			System.out.print("Kopa ::: ");
		else if(this.getPalo()==3)
			System.out.print("Espata ::: ");
		else if(this.getPalo()==4)
			System.out.print("Bastoa ::: ");
		System.out.println(this.getNumero());
		

	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getPalo() {
		return palo;
	}

	public void setPalo(int palo) {
		this.palo = palo;
	}

	public int getBaliotasuna() {
		return baliotasuna;
	}

	public void setBaliotasuna(int baliotasuna) {
		this.baliotasuna = baliotasuna;
	}

}
