
public class usuario {

	private String Izena;
	private int jokatuKop;
	private int presidenteKop;
	private int bicepresiKop;
	private int genteKop;
	private int bicemierdaKop;
	private int putamierdaKop;
	
	public usuario(String izena)
	{
		this.Izena=izena;
		this.jokatuKop=0;
		this.presidenteKop=0;
		this.bicepresiKop=0;
		this.genteKop=0;
		this.bicemierdaKop=0;
		this.putamierdaKop=0;
	}
	public usuario(String izena, int j, int p, int b, int g, int bi, int pu)
	{
		this.Izena=izena;
		this.jokatuKop=j;
		this.presidenteKop=p;
		this.bicepresiKop=b;
		this.genteKop=g;
		this.bicemierdaKop=bi;
		this.putamierdaKop=pu;
	}
	
	public void inprimatu()
	{
		System.out.println();
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println(this.getIzena());
		System.out.println("Jokatuak=> "+this.getJokatuKop());
		System.out.println("presik=> "+this.getPresidenteKop());
		System.out.println("Bicepresi=> "+this.getBicepresiKop());
		System.out.println("Gente=> "+this.getGenteKop());
		System.out.println("Bicemierda=> "+this.getBicemierdaKop());
		System.out.println("Putamierda=> "+this.getPutamierdaKop());
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println();
	}
	
	public String getIzena() {
		return Izena;
	}
	public void setIzena(String izena) {
		Izena = izena;
	}
	public int getJokatuKop() {
		return jokatuKop;
	}
	public void setJokatuKop(int jokatuKop) {
		this.jokatuKop = jokatuKop;
	}
	public int getPresidenteKop() {
		return presidenteKop;
	}
	public void setPresidenteKop(int presidenteKop) {
		this.presidenteKop = presidenteKop;
	}
	public int getBicepresiKop() {
		return bicepresiKop;
	}
	public void setBicepresiKop(int bicepresiKop) {
		this.bicepresiKop = bicepresiKop;
	}
	public int getGenteKop() {
		return genteKop;
	}
	public void setGenteKop(int genteKop) {
		this.genteKop = genteKop;
	}
	public int getBicemierdaKop() {
		return bicemierdaKop;
	}
	public void setBicemierdaKop(int bicemierdaKop) {
		this.bicemierdaKop = bicemierdaKop;
	}
	public int getPutamierdaKop() {
		return putamierdaKop;
	}
	public void setPutamierdaKop(int putamierdaKop) {
		this.putamierdaKop = putamierdaKop;
	}
}
