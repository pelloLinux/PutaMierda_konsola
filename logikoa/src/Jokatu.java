import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class Jokatu {

	private static String fitxategia="informazioa.txt";
	private static Baraja baraja = new Baraja();
	public static LinkedList<Jokalari> Jokalariak=new LinkedList<Jokalari>();

	@SuppressWarnings("unused")
	private static usuario usuario;
	private static int zenbanaka;
	private static int momentukoBaliotasuna;
	private static int irabaziDu;
	private static int posizioa;
	private static int kop;
	@SuppressWarnings("unused")
	private static int pliñek;
	private static boolean[] pliñ;
	private static int azkenaBotaDuena;
	private static LinkedList<Jokalari> klaseak=new LinkedList<Jokalari>();

	private static Jokalari player0=new Jokalari("player0");
	private static Jokalari player1=new Jokalari("player1");
	private static Jokalari player2=new Jokalari("player2");
	private static Jokalari player3=new Jokalari("player3");
	private static Jokalari player4=new Jokalari("player4");
	private static Jokalari player5=new Jokalari("player5");
	private static Jokalari player6=new Jokalari("player6");
	private static Jokalari player7=new Jokalari("player7");

	private static void inforIrakurri(String izena) throws FileNotFoundException
	{

		File fIzena = new File(izena);
		Scanner irakurlea = new Scanner(fIzena); // Fitxategia irakurtzeko
		// erabiltzen dugu.
		String lehenAldia=irakurlea.nextLine();
		if(lehenAldia.compareTo("0")==0)usuarioBerriaSortu();
		else
		{
			String[] Datuak = irakurlea.nextLine().split(","); 
			int j=Integer.parseInt(Datuak[1]);
			int p=Integer.parseInt(Datuak[2]);
			int b=Integer.parseInt(Datuak[3]);
			int g=Integer.parseInt(Datuak[4]);
			int bi=Integer.parseInt(Datuak[5]);
			int pu=Integer.parseInt(Datuak[6]);
			usuario=new usuario(Datuak[0], j, p, b, g, bi, pu);
		}
	}
	private static void usuarioBerriaSortu()
	{
		Scanner in=new Scanner(System.in);
		System.out.println("Nola da zure izena??");
		String izena=in.nextLine();
		usuario=new usuario(izena);
	}
	private static void fitxategianIdatzi(String Path) throws FileNotFoundException
	{
		File wrname = new File(Path); // Fitxategian idazteko erabiliko dugu.
		PrintWriter wrfile = new PrintWriter(wrname);
		wrfile.println("1");
		wrfile.print(usuario.getIzena()+",");
		wrfile.print(usuario.getJokatuKop()+",");
		wrfile.print(usuario.getPresidenteKop()+",");
		wrfile.print(usuario.getBicepresiKop()+",");
		wrfile.print(usuario.getGenteKop()+",");
		wrfile.print(usuario.getBicemierdaKop()+",");
		wrfile.print(usuario.getPutamierdaKop());
		
		wrfile.close();
	}
	private static void usuarioaEguneratu()
	{	
		usuario.setJokatuKop(usuario.getJokatuKop()+1);
		
		if(player0.getKlasea().compareTo("presi")==0)usuario.setPresidenteKop(usuario.getPresidenteKop()+1);
		else if(player0.getKlasea().compareTo("semipresi")==0)usuario.setBicepresiKop(usuario.getBicepresiKop()+1);
		else if(player0.getKlasea().compareTo("gente")==0)usuario.setGenteKop(usuario.getGenteKop()+1);
		else if(player0.getKlasea().compareTo("semiputamierda")==0)usuario.setBicemierdaKop(usuario.getBicemierdaKop()+1);
		else if(player0.getKlasea().compareTo("putamierda")==0)usuario.setPutamierdaKop(usuario.getPutamierdaKop()+1);
	}


	private static void ordenatu(Jokalari player){//Hautaketa bidezko metodoa
		int out,in,min;
		for(out=0; out<player.getBarajaZatia().size()-1;out++){
			min=out; // minimoaren indizea oraingoz
			for(in=out+1; in<player.getBarajaZatia().size(); in++){
				if(player.getBarajaZatia().get(in).getBaliotasuna() < player.getBarajaZatia().get(min).getBaliotasuna()){
					min=in;
					swap(player,out,min);
				}
			}
		}
	}

	private static void swap(Jokalari player, int one, int two){//aldagaien aldaketa egiteko soilik.


		Karta lag=player.getBarajaZatia().get(one);
		player.getBarajaZatia().set(one, player.getBarajaZatia().get(two));
		player.getBarajaZatia().set(two, lag);

	}


	@SuppressWarnings("unused")
	private static void eskukoAldaketa(){


		if(player1.isEskuko()){

			player1.setEskuko(false);
			player2.setEskuko(true);
			System.out.println("player2");
		}

		else if(player2.isEskuko()){

			player2.setEskuko(false);
			player3.setEskuko(true);
			System.out.println("player3");

		}
		else if(player3.isEskuko()){

			player3.setEskuko(false);
			player4.setEskuko(true);
			System.out.println("player4");
		}
		else if(player4.isEskuko()){

			player4.setEskuko(false);
			player1.setEskuko(true);
			System.out.println("player1");
		}
	}



	private static void hasiera() throws FileNotFoundException{

		inforIrakurri(fitxategia);
		usuario.inprimatu();
		baraja.NumeroAleatorios();
		baraja.Nahastu();

		player0=new Jokalari("player0",  player0.getKlasea());
		player1=new Jokalari("player1",  player1.getKlasea());
		player2=new Jokalari("player2",  player2.getKlasea());
		player3=new Jokalari("player3",  player3.getKlasea());
		player4=new Jokalari("player4",  player4.getKlasea());
		player5=new Jokalari("player5",  player5.getKlasea());
		player6=new Jokalari("player6",  player6.getKlasea());
		player7=new Jokalari("player7",  player7.getKlasea());

		klaseak=new LinkedList<Jokalari>();
		Jokalariak=new LinkedList<Jokalari>();


	}
	private static void jokalarikop(){
		// 3 jokalari gutxienez
		// 8 jokalri gehienez

		Scanner I=new Scanner(System.in);
		System.out.print("Zenbat jokalari? [4..8]");
		int x=I.nextInt();
		kop=x;

		if(x==3)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);

		}
		else if(x==4)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
		}
		else if(x==5)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
			Jokalariak.add(player4);
		}
		else if(x==6)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
			Jokalariak.add(player4);
			Jokalariak.add(player5);
		}
		else if(x==7)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
			Jokalariak.add(player4);
			Jokalariak.add(player5);
			Jokalariak.add(player6);
		}
		else if(x==8)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
			Jokalariak.add(player4);
			Jokalariak.add(player5);
			Jokalariak.add(player6);
			Jokalariak.add(player7);
		}




	}
	private static void jokalarikop2(){// bigarren aldiz jokatzen denerako.
		// 3 jokalari gutxienez
		// 8 jokalri gehienez


		int x=kop;


		if(x==4)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
		}
		else if(x==5)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
			Jokalariak.add(player4);
		}
		else if(x==6)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
			Jokalariak.add(player4);
			Jokalariak.add(player5);
		}
		else if(x==7)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
			Jokalariak.add(player4);
			Jokalariak.add(player5);
			Jokalariak.add(player6);
		}
		else if(x==8)
		{
			Jokalariak.add(player0);
			Jokalariak.add(player1);
			Jokalariak.add(player2);
			Jokalariak.add(player3);
			Jokalariak.add(player4);
			Jokalariak.add(player5);
			Jokalariak.add(player6);
			Jokalariak.add(player7);		
		}


	}


	@SuppressWarnings("unused")
	private static void kartakBanatu(){//bakoitzari tokatzen zaion kopurua, karta denak batera
		// berez ez da horrela partitzen

		int x=40/Jokalariak.size(); // jokalari bakoitzari partitu beharreko karta kopurua
		int y=0;
		int z=x;
		for(int i=0;i<Jokalariak.size();i++)
		{	
			if(40%Jokalariak.size()>i)//denentzat karta kopuru berdina tokatzen ez den kasuetan
			{
				for(int j=y;j<z+1;j++)
				{
					Jokalariak.get(i).getBarajaZatia().add(baraja.getBaraja().get(j));

				}
				y=z+1;
				z+=x+1;
			}
			else
			{
				for(int j=y;j<z;j++)
				{
					Jokalariak.get(i).getBarajaZatia().add(baraja.getBaraja().get(j));
				}
				y=z;
				z+=x;		
			}
		}		if(Jokatu.irabaziDu==-1)
			for(int i=0; i<Jokalariak.size(); i++)
			{
				ordenatu(Jokalariak.get(i));// jokalri bakoitzaren kartak ordenatu
			}

	}



	private static void kartakBanatu2()//kartak berez banatu behar diren bezala, banaka banaka.
	{
		int i=0;
		while(i<40)
		{
			Jokalariak.get(i%Jokalariak.size()).getBarajaZatia().add(baraja.getBaraja().get(i));
			i++;	
		}	
		for(int j=0; j<Jokalariak.size(); j++)ordenatu(Jokalariak.get(j));// jokalri bakoitzaren kartak ordenatu
		for(int j=0; j<Jokalariak.size(); j++)ordenatu(Jokalariak.get(j));// jokalri bakoitzaren kartak ordenatu
		for(int j=0; j<Jokalariak.size(); j++)ordenatu(Jokalariak.get(j));// jokalri bakoitzaren kartak ordenatu
	}



	private static void hirukoUrreaBilatu(){//jokatzen hiruko urrea hasten denez, lehendabizi hau bilatu behar.

		for(int i=0; i<Jokalariak.size(); i++)
		{
			for(int j=0; j<Jokalariak.get(i).getBarajaZatia().size(); j++)
			{
				if(Jokalariak.get(i).getBarajaZatia().get(j).getNumero()==3 && Jokalariak.get(i).getBarajaZatia().get(j).getPalo()==1)
				{
					Jokalariak.get(i).setEskuko(true);
					System.out.println(Jokalariak.get(zeinenTxanda()).getIzena());
					zenbanaka=zenbatHiruko(Jokalariak.get(i));
					momentukoBaliotasuna=Jokalariak.get(i).getBarajaZatia().get(j).getBaliotasuna();
					if(zenbanaka==1)
					{
						Jokalariak.get(i).getBarajaZatia().get(j).inprimatu();
						System.out.println();
						Jokalariak.get(i).getBarajaZatia().remove(j);
					}
					if(zenbanaka>1)
					{
						int[]indizeak=new int[4];
						indizeak[0]=-1;
						indizeak[1]=-1;
						indizeak[2]=-1;
						indizeak[3]=-1;
						zeinHirukoak(Jokalariak.get(i), indizeak);
						ezabatuInpr(Jokalariak.get(i), indizeak);
						System.out.println();
					}
				}
			}
		}
	}
	private static void ezabatuInpr(Jokalari J, int[] b)
	{
		if(b[0]!=-1)J.getBarajaZatia().get(b[0]).inprimatu();

		if(b[1]!=-1)J.getBarajaZatia().get(b[1]).inprimatu();

		if(b[2]!=-1)J.getBarajaZatia().get(b[2]).inprimatu();

		if(b[3]!=-1)J.getBarajaZatia().get(b[3]).inprimatu();

		if(b[0]!=-1)J.getBarajaZatia().remove(b[0]);

		if(b[1]!=-1)J.getBarajaZatia().remove(b[1]);

		if(b[2]!=-1)J.getBarajaZatia().remove(b[2]);

		if(b[3]!=-1)J.getBarajaZatia().remove(b[3]);

	}

	private static int zenbatHiruko(Jokalari y){

		int zenbat=0;

		for(int r=0; r<y.getBarajaZatia().size(); r++){

			if(y.getBarajaZatia().get(r).getNumero()==3){

				zenbat++;
			}
		}
		return zenbat;
	}

	private static int[] zeinHirukoak(Jokalari J, int[] indizeak)
	{
		int i=0;
		for(int r=0; r<J.getBarajaZatia().size(); r++){

			if(J.getBarajaZatia().get(r).getNumero()==3){

				indizeak[i]=r;
				i++;

			}
		}
		return indizeak;
	}


	private static int zeinenTxanda()
	{
		for(int i=0; i<Jokalariak.size(); i++)
		{
			if(Jokalariak.get(i).isEskuko()==true)return i;
		}
		return -1;
	}


	private static void txandaAldatu()
	{

		if(zeinenTxanda()==Jokalariak.size()-1||zeinenTxanda()==Jokalariak.size())
		{
			Jokalariak.get(zeinenTxanda()).setEskuko(false);
			Jokalariak.get(0).setEskuko(true);
		}
		else
		{
			int lag=zeinenTxanda();
			Jokalariak.get(zeinenTxanda()+1).setEskuko(true);
			if(lag!=-1)
				Jokalariak.get(lag).setEskuko(false);
		}

	}

	public static int numeroAleatorio(int x){//zenbakiak zoriz ateratzeko.
		return (int)(Math.random()*(x));//x-1 erarteko zenbakiak lortzen ditu
		// justu behar duguna
	}

	private static void jokoa1_lehenengoan() throws InterruptedException //lehenengo jokoaren lehenengo jaurtiketak
	{
		System.out.println();
		System.out.println();

		boolean a=true;// iterazioarentzat
		hirukoUrreaBilatu();// hiruko hurrea duena hasi da kartak botatzen
		txandaAldatu();
		//partida zatiak bukatzeko, pliñak egon badira eta ez badira egon desberdina denez.
		boolean[] bukatzeko=new boolean[Jokatu.Jokalariak.size()];
		int zenbat=0;
		pliñ=new boolean[Jokalariak.size()];
		pliñek=0;


		while(a==true)
		{
			Thread.sleep(1000);// exekuzioak kasu honetan segundu bat itxaron dezan.

			int txanda=zeinenTxanda();
			System.out.println(Jokalariak.get(txanda).getIzena());
			int lagBaliotasuna=momentukoBaliotasuna;//paso egon bada, berreskuratzeko.

			if(Jokalariak.get(txanda).getIzena()!="player0")
				momentukoBaliotasuna=Jokalariak.get(txanda).txanda(zenbanaka, momentukoBaliotasuna);//jokatzen hari denaren txanda EZ bada.
			else momentukoBaliotasuna=Jokalariak.get(txanda).nireTxanda(zenbanaka, momentukoBaliotasuna);// jokatzen hari denaren txanda bada.

			if(momentukoBaliotasuna==-1)//txanda zuen jokalariak paso egin badu.
			{
				pliñ[zeinenTxanda()]=false;//ez dago plin eginda
				momentukoBaliotasuna=lagBaliotasuna;//lehengo baliotasunera bueltatu.
				txandaAldatu();

				if(zenbat<=Jokalariak.size()-1)//jokalari guztiek paso eman badute, hala ere pliñ-ekin arazoak egon daitezke.
				{
					bukatzeko[zenbat]=true;
					zenbat++;
				}
			}
			else if(momentukoBaliotasuna==lagBaliotasuna)//jokalariak numero berdidekoa bota duenez, PLIÑ!!

			{
				pliñ[zeinenTxanda()]=false;// bota duena ez dago pliñ eginda
				azkenaBotaDuena=zeinenTxanda();// oraingoz jokalari hau da irabazlea.
				txandaAldatu();	
				pliñ[zeinenTxanda()]=true;// pliñ egin diotenez, markatu.
				txandaAldatu();
				System.out.println();
				System.out.println("***plin***");
				System.out.println();
				bukatzeko=new boolean[Jokatu.Jokalariak.size()];//paso inork egin gabe bezala jarri berriro ere.
				zenbat=0;
			}
			else if(momentukoBaliotasuna==-13)//biko urrea bota du txanda zuen Jokalariak.
			{
				//bikoUrreazBukatu=true;
				System.out.println(zeinenTxanda());
				azkenaBotaDuena=zeinenTxanda();
				break;// iterazioa bukatzeko momentuan.
			}
			else //mahaian zegoena baino handiagoa bota du txnda zuen jokalariak.
			{
				pliñ[zeinenTxanda()]=false;
				azkenaBotaDuena=zeinenTxanda();
				txandaAldatu();
				bukatzeko=new boolean[Jokatu.Jokalariak.size()];
				zenbat=0;
			}
			System.out.println();
			System.out.println();
			if(bukatuDa(bukatzeko) && pliñikEz(pliñ))a=false;//iterazioa bukatuko da.
		}
	}

	private static void jokoa1_hurrengoetan() throws InterruptedException // urrengo jokoetako lehenengo jaurtiketak.
	{

		System.out.println();
		System.out.println();
		Scanner in = new Scanner(System.in);
		denakFalsera();// inorren txanda ez dela jartzeko.
		momentukoBaliotasuna=0;
		zenbanaka=0;
		int i=putaMierda();// putamierda dena hasi da kartak botatzen
		Jokalariak.get(i).setEskuko(true);
		boolean[] bukatzeko=new boolean[Jokatu.Jokalariak.size()];
		int zenbat=0;
		pliñ=new boolean[Jokalariak.size()];
		pliñek=0;
		boolean a=true;


		while(a==true)
		{
			//Thread.sleep(1000);// exekuzioak kasu honetan segundu bat itxaron dezan.

			int txanda=zeinenTxanda();

			/**hasieretan bakarrik**/

			if(zenbanaka==0 && Jokalariak.get(txanda).getIzena()
					.compareTo("player0")!=0)zenbanaka=parejarik(Jokalariak.get(txanda));//hasterakoan parejarekin hasteko
			else if (zenbanaka==0 && Jokalariak.get(txanda).getIzena()
					.compareTo("player0")==0)
			{
				System.out.println("zenbanaka?");//jokatzen hari denarn txanda dezenz, zeuk aukeratu zenbat kartarekin hasi.
				zenbanaka=in.nextInt();
			}			
			/*******/

			System.out.println(Jokalariak.get(txanda).getIzena());
			int lagBaliotasuna=momentukoBaliotasuna;//paso egon bada, berreskuratzeko.

			if(Jokalariak.get(txanda).getIzena()!="player0")
				momentukoBaliotasuna=Jokalariak.get(txanda).txanda(zenbanaka, momentukoBaliotasuna);//jokatzen hari denaren txanda EZ bada.
			else momentukoBaliotasuna=Jokalariak.get(txanda).nireTxanda(zenbanaka, momentukoBaliotasuna);// jokatzen hari denaren txanda bada.

			if(momentukoBaliotasuna==-1)//txanda zuen jokalariak paso egin badu.
			{
				pliñ[zeinenTxanda()]=false;//ez dago plin eginda
				momentukoBaliotasuna=lagBaliotasuna;//lehengo baliotasunera bueltatu.
				txandaAldatu();

				if(zenbat<=Jokalariak.size()-1)//jokalari guztiek paso eman badute, hala ere pliñ-ekin arazoak egon daitezke.
				{
					bukatzeko[zenbat]=true;
					zenbat++;
				}
			}
			else if(momentukoBaliotasuna==lagBaliotasuna)//jokalariak numero berdidekoa bota duenez, PLIÑ!!

			{
				pliñ[zeinenTxanda()]=false;// bota duena ez dago pliñ eginda
				azkenaBotaDuena=zeinenTxanda();// oraingoz jokalari hau da irabazlea.
				txandaAldatu();	
				pliñ[zeinenTxanda()]=true;// pliñ egin diotenez, markatu.
				txandaAldatu();
				System.out.println();
				System.out.println("***plin***");
				System.out.println();
				bukatzeko=new boolean[Jokatu.Jokalariak.size()];//paso inork egin gabe bezala jarri berriro ere.
				zenbat=0;
			}
			else if(momentukoBaliotasuna==-13)//biko urrea bota du txanda zuen Jokalariak.
			{
				//bikoUrreazBukatu=true;
				System.out.println(zeinenTxanda());
				azkenaBotaDuena=zeinenTxanda();
				break;// iterazioa bukatzeko momentuan.
			}
			else //mahaian zegoena baino handiagoa bota du txnda zuen jokalariak.
			{
				pliñ[zeinenTxanda()]=false;
				azkenaBotaDuena=zeinenTxanda();
				txandaAldatu();
				bukatzeko=new boolean[Jokatu.Jokalariak.size()];
				zenbat=0;
			}

			System.out.println();
			System.out.println();
			if(bukatuDa(bukatzeko) && pliñikEz(pliñ))a=false;//iterazioa bukatuko da.
		}
	}
	private static void jokoa2() throws InterruptedException
	{

		Scanner in = new Scanner(System.in);
		int txanda=azkenaBotaDuena;
		posizioa=0;


		while(Jokalariak.size()>1) 
		{
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println();
			System.out.println();
			denakFalsera();
			txanda=azkenaBotaDuena;
			Jokalariak.get(txanda).setEskuko(true);
			zenbanaka=0;
			momentukoBaliotasuna=0;
			boolean[] bukatzeko=new boolean[Jokatu.Jokalariak.size()];
			int zenbat=0;
			pliñ=new boolean[Jokalariak.size()];
			pliñek=0;
			boolean a=true;
			boolean b=true;

			while(a==true)
			{

				Thread.sleep(1000);// exekuzioak kasu honetan segundu bat itxaron dezan.
				if(b==false)
					txanda=zeinenTxanda();

				b=false;

				if(zenbanaka==0 && Jokalariak.get(txanda).getIzena()
						.compareTo("player0")!=0)zenbanaka=parejarik(Jokalariak.get(txanda));//hasterakoan parejarekin hasteko
				else if (zenbanaka==0 && Jokalariak.get(txanda).getIzena()
						.compareTo("player0")==0){
					System.out.println("zenbanaka?");
					zenbanaka=in.nextInt();
				}
				System.out.println("zenbanaka"+zenbanaka);
				System.out.println(Jokalariak.get(txanda).getIzena());
				int lagBaliotasuna=momentukoBaliotasuna;

				if(Jokalariak.get(txanda).getIzena()!="player0")
				{	
					momentukoBaliotasuna=Jokalariak.get(txanda).txanda(zenbanaka, momentukoBaliotasuna);
				}
				else momentukoBaliotasuna=Jokalariak.get(txanda).nireTxanda(zenbanaka, momentukoBaliotasuna);

				if(momentukoBaliotasuna==-1)
				{
					pliñ[zeinenTxanda()]=false;
					momentukoBaliotasuna=lagBaliotasuna;
					txandaAldatu();
					if(zenbat<=Jokalariak.size()-1)
					{
						bukatzeko[zenbat]=true;
						zenbat++;
					}

				}
				else if(momentukoBaliotasuna==lagBaliotasuna)
				{
					int irabazlea=zeinenTxanda();
					pliñ[zeinenTxanda()]=false;
					azkenaBotaDuena=zeinenTxanda();
					if(kartakBukatu(irabazlea, posizioa)==1)
					{
						if(azkenaBotaDuena==Jokalariak.size())azkenaBotaDuena=0;

					}

					txandaAldatu();
					txandaAldatu();
					posizioa=klaseak.size();
					System.out.println();
					System.out.println("***plin***");
					System.out.println();
					bukatzeko=new boolean[Jokatu.Jokalariak.size()];
					zenbat=0;
				}
				else if(momentukoBaliotasuna==-13)//biko urrea bota du txanda zuen Jokalariak.
				{
					//bikoUrreazBukatu=true;
					System.out.println(zeinenTxanda());
					azkenaBotaDuena=zeinenTxanda();
					if(kartakBukatu(zeinenTxanda(), posizioa)==1)
					{
						if(azkenaBotaDuena==Jokalariak.size() || azkenaBotaDuena==Jokalariak.size()-1)azkenaBotaDuena=0;
						else azkenaBotaDuena++;
					}
					break;// iterazioa bukatzeko momentuan.
				}
				else 
				{
					int irabazlea=zeinenTxanda();
					pliñ[zeinenTxanda()]=false;
					azkenaBotaDuena=zeinenTxanda();
					txandaAldatu();
					posizioa=klaseak.size();
					if(kartakBukatu(irabazlea, posizioa)==1)
					{
						if(azkenaBotaDuena==Jokalariak.size())azkenaBotaDuena=0;

					}
					bukatzeko=new boolean[Jokatu.Jokalariak.size()];
					zenbat=0;
				}
				System.out.println();
				System.out.println();
				if(bukatuDa(bukatzeko) && pliñikEz(pliñ))a=false;
				zenbat4();
			}
		}
		Jokalariak.get(0).setKlasea("putamierda");//listan geratu den azken jokalaria "putamierda" izango da.
		klaseak.add(Jokalariak.get(0));
		System.out.println();
		klaseakInprimatu();
	}

	private static int kartakBukatu(int irabazlea, int posizioa)
	{
		if(Jokalariak.get(irabazlea).getBarajaZatia().size()==0)
		{
			int irabazi=irabazlea;
			klaseak.add(Jokalariak.get(irabazi));
			if(posizioa==0)Jokalariak.get(irabazi).setKlasea("presi");
			else if(posizioa==1)Jokalariak.get(irabazi).setKlasea("semipresi");
			else if(Jokalariak.size()==2)Jokalariak.get(irabazi).setKlasea("semiputamierda");
			else Jokalariak.get(irabazi).setKlasea("gente");
			System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
			System.out.println(Jokalariak.get(irabazi).getIzena()+" ==> "+Jokalariak.get(irabazi).getKlasea());
			System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
			Jokalariak.remove(irabazi);
			return 1;

		}
		return 0;

	}

	private static boolean bukatuDa(boolean[] b)
	{
		for (int i = 0; i < b.length-1; i++) {
			if(b[i]==false)return false;
		}
		return true;
	}

	private static boolean pliñikEz(boolean[] b)
	{
		for (int i = 0; i < b.length-1; i++) {
			if(b[i]==true)return false;
		}
		return true;
	}

	public static void jokatuLehenengoan() throws InterruptedException, FileNotFoundException
	{
		hasiera();
		jokalarikop();
		kartakBanatu2();
		Nagusia.proba();
		jokoa1_lehenengoan();
		jokoa2();
		usuarioaEguneratu();
		fitxategianIdatzi(fitxategia);
	}

	public static void jokatzenSegi() throws InterruptedException, FileNotFoundException
	{

		Jokalariak=new LinkedList<Jokalari>();
		boolean a=true;
		while(a==true)
		{
			System.out.println();
			System.out.println("****************************");
			System.out.println("****************************");
			System.out.println();
			System.out.println("Jokatzen jarraitu nahi al duzu(bai(b)/ez(e))");

			Thread.sleep(3000);
			Scanner in= new Scanner(System.in);
			String x=in.nextLine();
			if(x.compareTo("b")==0)
			{
				Jokatu.hasiera();
				Jokatu.jokalarikop2();
				Jokatu.kartakBanatu2();
				Nagusia.proba();
				klaseanJerarkia();
				for(int i=0; i<Jokalariak.size(); i++)ordenatu(Jokalariak.get(i));// jokalri bakoitzaren kartak ordenatu
				Nagusia.proba();
				klaseakInprimatu();
				Jokatu.jokoa1_hurrengoetan();
				Jokatu.jokoa2();
				usuarioaEguneratu();
				fitxategianIdatzi(fitxategia);

			}
			else
			{
				System.out.println();
				System.out.println("AMAIERA");
				a=false;
			}

		}
	}

	private static void klaseanJerarkia()
	{
		int a=0;
		int b=0;
		int c=0;
		int d=0;
		LinkedList<Karta> txarrenak=new LinkedList<Karta>();
		LinkedList<Karta> txarrena =new LinkedList<Karta>();
		LinkedList<Karta> onenak=new LinkedList<Karta>();
		LinkedList<Karta> onena=new LinkedList<Karta>();

		for(int i=0;i<Jokalariak.size(); i++){

			if(Jokalariak.get(i).getKlasea().compareTo("putamierda")==0)
			{
				onenak=onenak(Jokalariak.get(i));
				a=i;
				System.out.println("Onenak-"+Jokalariak.get(i).getIzena()+"-"+Jokalariak.get(i).getKlasea());
				onenak.getLast().inprimatu();
				onenak.getFirst().inprimatu();
				System.out.println();

			}
			else if(Jokalariak.get(i).getKlasea().compareTo("semiputamierda")==0)
			{
				onena=onena(Jokalariak.get(i));
				b=i;
				System.out.println("Onena-"+Jokalariak.get(i).getIzena()+"-"+Jokalariak.get(i).getKlasea());
				onena.getFirst().inprimatu();
				System.out.println();
			}
			else if(Jokalariak.get(i).getKlasea().compareTo("presi")==0)
			{
				txarrenak=txarrenak(Jokalariak.get(i));
				c=i;
				System.out.println("txarrenak-"+Jokalariak.get(i).getIzena()+"-"+Jokalariak.get(i).getKlasea());
				txarrenak.getLast().inprimatu();
				txarrenak.getFirst().inprimatu();
				System.out.println();
			}
			else if(Jokalariak.get(i).getKlasea().compareTo("semipresi")==0)
			{
				txarrena=txarrena(Jokalariak.get(i));
				d=i;
				System.out.println("txarrena-"+Jokalariak.get(i).getIzena()+"-"+Jokalariak.get(i).getKlasea());
				txarrena.getFirst().inprimatu();
				System.out.println();
			}
		}


		Jokalariak.get(c).getBarajaZatia().add(onenak.getLast());//putamierdan onenak presidentintzat
		Jokalariak.get(c).getBarajaZatia().add(onenak.getFirst());
		Jokalariak.get(a).getBarajaZatia().add(txarrenak.getLast());//presiyen txarrenak putamierdintzat
		Jokalariak.get(a).getBarajaZatia().add(txarrenak.getFirst());
		Jokalariak.get(d).getBarajaZatia().add(onena.get(0));// semiputan onena semipresiyentzat
		Jokalariak.get(b).getBarajaZatia().add(txarrena.get(0));// semipresin txarrena semimierdantzat

		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::");
		System.out.println();
		System.out.println();
		System.out.println("::::::::::::::::KARTAK ORDEZKATZEN::::::::::::::::");
		System.out.println();
		System.out.println();
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::");

	}
	private static LinkedList<Karta> onenak(Jokalari J)
	{
		int max=-1;
		int indMax=-1;
		LinkedList<Karta> onenak=new LinkedList<Karta>();
		for(int i=0; i< J.getBarajaZatia().size(); i++)
		{
			if(J.getBarajaZatia().get(i).getBaliotasuna()>=max)
			{
				max=J.getBarajaZatia().get(i).getBaliotasuna();
				indMax=i;
			}
			if(J.getBarajaZatia().get(i).getNumero()==3 && J.getBarajaZatia().get(i).getPalo()==1)
			{
				indMax=i;
				break;
			}
		}
		onenak.add(J.getBarajaZatia().get(indMax));
		J.getBarajaZatia().remove(indMax);


		max=-1;
		for(int i=0; i< J.getBarajaZatia().size(); i++)
		{
			if(J.getBarajaZatia().get(i).getBaliotasuna()>=max)
			{
				max=J.getBarajaZatia().get(i).getBaliotasuna();
				indMax=i;
			}

		}
		onenak.add(J.getBarajaZatia().get(indMax));
		J.getBarajaZatia().remove(indMax);
		return onenak;
	}

	private static LinkedList<Karta> onena(Jokalari J)
	{
		int max=-1;
		int indMax=0;
		LinkedList<Karta> onena=new LinkedList<Karta>();
		for(int i=0; i< J.getBarajaZatia().size(); i++)
		{
			if(J.getBarajaZatia().get(i).getBaliotasuna()>=max)
			{
				max=J.getBarajaZatia().get(i).getBaliotasuna();
				indMax=i;
			}

		}
		onena.add(J.getBarajaZatia().get(indMax));
		J.getBarajaZatia().remove(indMax);
		return onena;
	}

	private static LinkedList<Karta> txarrenak(Jokalari J)
	{
		int min=13;
		int indMin=0;
		LinkedList<Karta> txarrenak=new LinkedList<Karta>();
		for(int i=0; i< J.getBarajaZatia().size(); i++)
		{
			if(J.getBarajaZatia().get(i).getBaliotasuna()<=min)
			{
				min=J.getBarajaZatia().get(i).getBaliotasuna();
				indMin=i;
			}

		}
		txarrenak.add(J.getBarajaZatia().get(indMin));
		J.getBarajaZatia().remove(indMin);
		for(int i=0; i< J.getBarajaZatia().size(); i++)
		{
			if(J.getBarajaZatia().get(i).getBaliotasuna()<=min)
			{
				min=J.getBarajaZatia().get(i).getBaliotasuna();
				indMin=i;
			}

		}
		txarrenak.add(J.getBarajaZatia().get(indMin));
		J.getBarajaZatia().remove(indMin);
		return txarrenak;
	}
	private static LinkedList<Karta> txarrena(Jokalari J)
	{
		int min=13;
		int indMin=0;
		LinkedList<Karta> txarrena=new LinkedList<Karta>();
		for(int i=0; i< J.getBarajaZatia().size(); i++)
		{
			if(J.getBarajaZatia().get(i).getBaliotasuna()<=min)
			{
				min=J.getBarajaZatia().get(i).getBaliotasuna();
				indMin=i;
			}

		}
		txarrena.add(J.getBarajaZatia().get(indMin));
		J.getBarajaZatia().remove(indMin);
		return txarrena;
	}
	private static int parejarik(Jokalari J)
	{
		boolean a=false;
		int bilatua=-1;
		for(int i=0; i<J.getBarajaZatia().size(); i++)
		{
			int pos=0;
			bilatua=J.getBarajaZatia().get(i).getNumero();
			for(int j=i+1; j<J.getBarajaZatia().size(); j++)
			{
				if(J.getBarajaZatia().get(j).getNumero()==bilatua)
				{
					a=true;
					if(pos==0)pos+=2;
					else pos++;
				}
			}
			if(a==true)return pos;
		}
		return 1;
	}

	private static void klaseakInprimatu()
	{
		System.out.println("p0---"+player0.getKlasea());
		System.out.println("p1---"+player1.getKlasea());
		System.out.println("p2---"+player2.getKlasea());
		System.out.println("p3---"+player3.getKlasea());
		System.out.println("p4---"+player4.getKlasea());
		System.out.println("p5---"+player5.getKlasea());
		System.out.println("p6---"+player6.getKlasea());
		System.out.println("p7---"+player7.getKlasea());

	}
	private static void zenbat4()
	{
		System.out.println("p0---"+player0.getBarajaZatia().size());
		System.out.println("p1---"+player1.getBarajaZatia().size());
		System.out.println("p2---"+player2.getBarajaZatia().size());
		System.out.println("p3---"+player3.getBarajaZatia().size());
	}

	private static void denakFalsera()
	{
		for(int i=0; i<Jokalariak.size(); i++)Jokalariak.get(i).setEskuko(false);

	}
	private static int putaMierda()
	{
		for(int i=0; i<Jokalariak.size(); i++)if(Jokalariak.get(i).getKlasea()=="putamierda")return i;
		return -1;
	}

}