import java.util.ArrayList;
import java.util.LinkedList;

public class Baraja {

	private LinkedList<Karta> Baraja;

	public Baraja(){
		
		Baraja=new LinkedList<Karta>();
		
		//urreak
		Karta u1 = new Karta(1,1,9);
		Karta u2 = new Karta(1,2,10);
		Karta u3 = new Karta(1,3,1);
		Karta u4 = new Karta(1,4,2);
		Karta u5 = new Karta(1,5,3);
		Karta u6 = new Karta(1,6,4);
		Karta u7 = new Karta(1,7,5);
		Karta u10 = new Karta(1,10,6);
		Karta u11 = new Karta(1,11,7);
		Karta u12 = new Karta(1,12,8);
		//kopak
		Karta k1 = new Karta(2,1,9);
		Karta k2 = new Karta(2,2,10);
		Karta k3 = new Karta(2,3,1);
		Karta k4 = new Karta(2,4,2);
		Karta k5 = new Karta(2,5,3);
		Karta k6 = new Karta(2,6,4);
		Karta k7 = new Karta(2,7,5);
		Karta k10 = new Karta(2,10,6);
		Karta k11 = new Karta(2,11,7);
		Karta k12 = new Karta(2,12,8);
		//espatak
		Karta e1 = new Karta(3,1,9);
		Karta e2 = new Karta(3,2,10);
		Karta e3 = new Karta(3,3,1);
		Karta e4 = new Karta(3,4,2);
		Karta e5 = new Karta(3,5,3);
		Karta e6 = new Karta(3,6,4);
		Karta e7 = new Karta(3,7,5);
		Karta e10 = new Karta(3,10,6);
		Karta e11 = new Karta(3,11,7);
		Karta e12 = new Karta(3,12,8);
		//bastoak
		Karta b1 = new Karta(4,1,9);
		Karta b2 = new Karta(4,2,10);
		Karta b3 = new Karta(4,3,1);
		Karta b4 = new Karta(4,4,2);
		Karta b5 = new Karta(4,5,3);
		Karta b6 = new Karta(4,6,4);
		Karta b7 = new Karta(4,7,5);
		Karta b10 = new Karta(4,10,6);
		Karta b11 = new Karta(4,11,7);
		Karta b12 = new Karta(4,12,8);

		Baraja.add(u1);
		Baraja.add(u2);
		Baraja.add(u3);
		Baraja.add(u4);
		Baraja.add(u5);
		Baraja.add(u6);
		Baraja.add(u7);
		Baraja.add(u10);
		Baraja.add(u11);
		Baraja.add(u12);

		Baraja.add(k1);
		Baraja.add(k2);
		Baraja.add(k3);
		Baraja.add(k4);
		Baraja.add(k5);
		Baraja.add(k6);
		Baraja.add(k7);
		Baraja.add(k10);
		Baraja.add(k11);
		Baraja.add(k12);

		Baraja.add(e1);
		Baraja.add(e2);
		Baraja.add(e3);
		Baraja.add(e4);
		Baraja.add(e5);
		Baraja.add(e6);
		Baraja.add(e7);
		Baraja.add(e10);
		Baraja.add(e11);
		Baraja.add(e12);

		Baraja.add(b1);
		Baraja.add(b2);
		Baraja.add(b3);
		Baraja.add(b4);
		Baraja.add(b5);
		Baraja.add(b6);
		Baraja.add(b7);
		Baraja.add(b10);
		Baraja.add(b11);
		Baraja.add(b12);

	}

	@SuppressWarnings("rawtypes")
	private ArrayList listaNumero;

	@SuppressWarnings({ "rawtypes" })
	public void NumeroAleatorios(){

		setListaNumero(new ArrayList());
	}

	private int numeroAleatorio(){
		return (int)(Math.random()*(40));
	}

	@SuppressWarnings("unchecked")
	private int generar(){
		if(getListaNumero().size() < (42) +1){
			//Aun no se han generado todos los numeros
			int numero = numeroAleatorio();//genero un numero
			if(getListaNumero().isEmpty()){//si la lista esta vacia
				getListaNumero().add(numero);
				return numero;
			}else{//si no esta vacia
				if(getListaNumero().contains(numero)){//Si el numero que generé esta contenido en la lista
					return generar();//recursivamente lo mando a generar otra vez
				}else{//Si no esta contenido en la lista
					getListaNumero().add(numero);
					return numero;
				}
			}
		}else{// ya se generaron todos los numeros
			return -1;
		}
	}

	
	public void Nahastu()
	{
		LinkedList<Karta> lag = this.Baraja;
		this.Baraja=new LinkedList<Karta>();
		for(int i=0; i<40; i++ )
		{
			this.Baraja.add(lag.get(generar()));
		}
	}

	@SuppressWarnings("rawtypes")
	public ArrayList getListaNumero() {
		return listaNumero;
	}

	public void setListaNumero(@SuppressWarnings("rawtypes") ArrayList listaNumero) {
		this.listaNumero = listaNumero;
	}

	public LinkedList<Karta> getBaraja(){
		
		return this.Baraja;
	}


}
