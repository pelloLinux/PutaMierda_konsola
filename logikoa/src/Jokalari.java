import java.util.LinkedList;
import java.util.Scanner;


public class Jokalari {


	private String Izena;
	private boolean Eskuko;
	private LinkedList<Karta> BarajaZatia;
	private String klasea;


	public Jokalari(String Izena){

		this.setIzena(Izena);
		this.setEskuko(false);
		this.setBarajaZatia(new LinkedList<Karta>());

	}
	public Jokalari(String Izena, String klasea){

		this.setIzena(Izena);
		this.setEskuko(false);
		this.setBarajaZatia(new LinkedList<Karta>());
		this.setKlasea(klasea);

	}


	public String getIzena() {
		return Izena;
	}


	public void setIzena(String izena) {
		Izena = izena;
	}


	public boolean isEskuko() {
		return Eskuko;
	}


	public void setEskuko(boolean eskuko) {
		Eskuko = eskuko;
	}


	public LinkedList<Karta> getBarajaZatia() {
		return BarajaZatia;
	}


	public void setBarajaZatia(LinkedList<Karta> linkedList) {
		BarajaZatia = linkedList;
	}

	public int txanda(int zenbanaka, int momendtukoBalioa)//-1 bueltatzea paso egitea da
	{	
		int[] indizeak1=new int[1];
		boolean aurkitua=false;
		int i=0;
		if(zenbanaka==1)
		{
			while(i<this.BarajaZatia.size() && aurkitua==false)
			{
				if(parejaAldu(this.getBarajaZatia().get(i), this))
				{
					if(this.BarajaZatia.get(i).getBaliotasuna()>=momendtukoBalioa)
					{
						aurkitua=true;
						if(false){//botaBaioEz(momendtukoBalioa, zenbanaka, this, this.getBarajaZatia().get(i), indizeak1)==false){
							System.out.println("paso%");
							return -1; // botatzea merezi duen edo ez...
						}
						int mahaira_botatakoa=this.getBarajaZatia().get(i).getBaliotasuna();
						if(this.getBarajaZatia().get(i).getNumero()==2 && this.getBarajaZatia().get(i).getPalo()==1)
						{
							this.BarajaZatia.get(i).inprimatu();
							this.BarajaZatia.remove(i);
							return -13;
						}
						else
						{
							this.BarajaZatia.get(i).inprimatu();
							this.BarajaZatia.remove(i);
							return mahaira_botatakoa;
						}
					}

				}	
				i++;
			}
			System.out.println("paso");
			return -1;
		}
		else if(zenbanaka==2)
		{
			i=0;
			int indizeak[]= new int[2];

			while(i<this.BarajaZatia.size())
			{
				if(triorikBai(this.getBarajaZatia().get(i), this))
				{
					if(this.BarajaZatia.get(i).getBaliotasuna()>=momendtukoBalioa && zenbat(this.BarajaZatia.get(i).getNumero())==2)
					{
						int mahaira_botatakoa=this.getBarajaZatia().get(i).getBaliotasuna();
						indizeak(indizeak, this.BarajaZatia.get(i).getNumero());
						if(botaBaioEz(momendtukoBalioa, zenbanaka, this, this.getBarajaZatia().get(i), indizeak)==false)
						{
							if(momendtukoBalioa==-1)
							{
								zenbanaka=1;
								int j=0;
								while(j<this.BarajaZatia.size() && aurkitua==false)
								{
									if(this.BarajaZatia.get(j).getBaliotasuna()>=momendtukoBalioa)
									{
										aurkitua=true;
										if(botaBaioEz(momendtukoBalioa, zenbanaka, this, this.getBarajaZatia().get(j), indizeak1)==false){
											System.out.println("paso%");
											return -1; // botatzea merezi duen edo ez...
										}
										mahaira_botatakoa=this.getBarajaZatia().get(j).getBaliotasuna();
										if(this.getBarajaZatia().get(j).getNumero()==2 && this.getBarajaZatia().get(j).getPalo()==1)
										{
											this.BarajaZatia.get(j).inprimatu();
											this.BarajaZatia.remove(j);
											return -13;
										}
										else
										{
											this.BarajaZatia.get(j).inprimatu();
											this.BarajaZatia.remove(j);
											return mahaira_botatakoa;
										}

									}	
									j++;
								}
								System.out.println("paso");
								return -1;

							}
							else{
								System.out.println("paso");
								return -1; // botatzea merezi duen edo ez...
							}

						}
						else
						{
							this.getBarajaZatia().get(indizeak[0]).inprimatu();
							this.getBarajaZatia().get(indizeak[1]).inprimatu();
							this.getBarajaZatia().remove(indizeak[0]);
							this.getBarajaZatia().remove(indizeak[1]-1);
							return mahaira_botatakoa;
						}
					}
				}		
				i++;
			}
			int x=BikoUrreaBadu(this);
			if(x!=-1)
				if(bikoUrrieBotaBaiOez(zenbanaka, momendtukoBalioa)==true)
				{
					this.BarajaZatia.get(x).inprimatu();
					this.BarajaZatia.remove(x);
					return -13;
				}

			System.out.println("paso");
			return -1;
		}
		else if(zenbanaka==3)
		{
			i=0;
			int indizeak3[]= new int[3];

			while(i<this.BarajaZatia.size())
			{
				if(this.BarajaZatia.get(i).getBaliotasuna()>=momendtukoBalioa && zenbat(this.BarajaZatia.get(i).getNumero())==3)
				{
					int mahaira_botatakoa=this.getBarajaZatia().get(i).getBaliotasuna();
					indizeak(indizeak3, this.BarajaZatia.get(i).getNumero());
					if(botaBaioEz(momendtukoBalioa, zenbanaka, this, this.getBarajaZatia().get(i), indizeak3)==false)
					{
						if(momendtukoBalioa==-1)
						{
							zenbanaka=1;
							int j=0;
							while(j<this.BarajaZatia.size() && aurkitua==false)
							{
								if(this.BarajaZatia.get(j).getBaliotasuna()>=momendtukoBalioa)
								{
									aurkitua=true;
									if(botaBaioEz(momendtukoBalioa, zenbanaka, this, this.getBarajaZatia().get(j), indizeak1)==false){
										System.out.println("paso%");
										return -1; // botatzea merezi duen edo ez...
									}
									mahaira_botatakoa=this.getBarajaZatia().get(j).getBaliotasuna();
									if(this.getBarajaZatia().get(j).getNumero()==2 && this.getBarajaZatia().get(j).getPalo()==1)
									{
										this.BarajaZatia.get(j).inprimatu();
										this.BarajaZatia.remove(j);
										return -13;
									}
									else
									{
										this.BarajaZatia.get(j).inprimatu();
										this.BarajaZatia.remove(j);
										return mahaira_botatakoa;
									}

								}	
								j++;
							}
							System.out.println("paso");
							return -1;

						}
						else{
							System.out.println("paso");
							return -1; // botatzea merezi duen edo ez...
						}
					}
					else
					{
						this.getBarajaZatia().get(indizeak3[0]).inprimatu();
						this.getBarajaZatia().get(indizeak3[1]).inprimatu();
						this.getBarajaZatia().get(indizeak3[2]).inprimatu();
						this.getBarajaZatia().remove(indizeak3[0]);
						this.getBarajaZatia().remove(indizeak3[1]-1);
						this.getBarajaZatia().remove(indizeak3[2]-2);
						return mahaira_botatakoa;
					}

				}	
				i++;
			}
			int x=BikoUrreaBadu(this);
			if(x!=-1)
				if(bikoUrrieBotaBaiOez(zenbanaka, momendtukoBalioa)==true)
				{
					this.BarajaZatia.get(x).inprimatu();
					this.BarajaZatia.remove(x);
					return -13;
				}
			System.out.println("paso");
			return -1;
		}

		else if(zenbanaka==4)
		{
			i=0;
			int indizeak4[]= new int[4];

			while(i<this.BarajaZatia.size())
			{
				if(this.BarajaZatia.get(i).getBaliotasuna()>=momendtukoBalioa && zenbat(this.BarajaZatia.get(i).getNumero())==4)
				{
					int mahaira_botatakoa=this.getBarajaZatia().get(i).getBaliotasuna();
					indizeak(indizeak4, this.BarajaZatia.get(i).getNumero());
					if(botaBaioEz(momendtukoBalioa, zenbanaka, this, this.getBarajaZatia().get(i), indizeak4)==false)
					{
						if(momendtukoBalioa==-1)
						{
							zenbanaka=1;
							int j=0;
							while(j<this.BarajaZatia.size() && aurkitua==false)
							{
								if(this.BarajaZatia.get(j).getBaliotasuna()>=momendtukoBalioa)
								{
									aurkitua=true;
									if(botaBaioEz(momendtukoBalioa, zenbanaka, this, this.getBarajaZatia().get(j), indizeak1)==false){
										System.out.println("paso%");
										return -1; // botatzea merezi duen edo ez...
									}
									mahaira_botatakoa=this.getBarajaZatia().get(j).getBaliotasuna();
									if(this.getBarajaZatia().get(j).getNumero()==2 && this.getBarajaZatia().get(j).getPalo()==1)
									{
										this.BarajaZatia.get(j).inprimatu();
										this.BarajaZatia.remove(j);
										return -13;
									}
									else
									{
										this.BarajaZatia.get(j).inprimatu();
										this.BarajaZatia.remove(j);
										return mahaira_botatakoa;
									}

								}	
								j++;
							}
							System.out.println("paso");
							return -1;

						}
						else{
							System.out.println("paso");
							return -1; // botatzea merezi duen edo ez...
						}
					}
					else
					{
						this.getBarajaZatia().get(indizeak4[0]).inprimatu();
						this.getBarajaZatia().get(indizeak4[1]).inprimatu();
						this.getBarajaZatia().get(indizeak4[2]).inprimatu();
						this.getBarajaZatia().get(indizeak4[3]).inprimatu();
						this.getBarajaZatia().remove(indizeak4[0]);
						this.getBarajaZatia().remove(indizeak4[1]-1);
						this.getBarajaZatia().remove(indizeak4[2]-2);
						this.getBarajaZatia().remove(indizeak4[3]-3);
						return mahaira_botatakoa;
					}

				}	
				i++;
			}
			int x=BikoUrreaBadu(this);
			if(x!=-1)
				if(bikoUrrieBotaBaiOez(zenbanaka, momendtukoBalioa)==true)
				{
					this.BarajaZatia.get(x).inprimatu();
					this.BarajaZatia.remove(x);
					return -13;
				}
			System.out.println("paso");
			return -1;
		}
		System.out.println("paso");
		return -1;
	}


	private void indizeak(int[] b, int x)
	{
		int a=0;
		for (int i = 0; i < this.getBarajaZatia().size(); i++)
		{
			if(this.getBarajaZatia().get(i).getNumero()==x)
			{
				b[a]=i;
				a++;
			}

		}
	}

	private int zenbat(int x)
	{
		int a=0;
		for (int i = 0;i<this.getBarajaZatia().size() ; i++)
		{
			if(this.getBarajaZatia().get(i).getNumero()==x)
			{
				a++;
			}

		}
		return a;
	}


	public int nireTxanda(int zenbanaka, int momentukoBalioa)
	{
		//zenbatekoa 1 baino handiagoa denenan, zenbakien indizeak txikienetik handienera eman behar dira(horaingoz)
		if(zenbanaka==1)
		{
			System.out.println("-1"+"--> paso");
			for(int i=0; i<this.getBarajaZatia().size(); i++)
			{
				System.out.print(i+"--> ");
				this.getBarajaZatia().get(i).inprimatu();
			}

			Scanner in=new Scanner(System.in);
			int x=in.nextInt();

			if(x!=-1 && this.getBarajaZatia().get(x).getBaliotasuna()>=momentukoBalioa)	
			{

				int mahaira_botatakoa=this.getBarajaZatia().get(x).getBaliotasuna();
				System.out.println();
				if(this.getBarajaZatia().get(x).getNumero()==2 && this.getBarajaZatia().get(x).getPalo()==1)
				{
					this.BarajaZatia.get(x).inprimatu();
					this.BarajaZatia.remove(x);
					return -13;
				}
				else
				{
					this.BarajaZatia.get(x).inprimatu();
					this.BarajaZatia.remove(x);
					return mahaira_botatakoa;
				}

			}

		}

		else if(zenbanaka==2)
		{
			System.out.println("-1"+"--> paso");
			for(int i=0; i<this.getBarajaZatia().size(); i++)
			{
				System.out.print(i+"--> ");
				this.getBarajaZatia().get(i).inprimatu();
			}

			Scanner in=new Scanner(System.in);
			int x=in.nextInt();
			if(x==-1)return -1;
			else if(this.getBarajaZatia().get(x).getNumero()==2 && this.getBarajaZatia().get(x).getPalo()==1)
			{
				this.BarajaZatia.get(x).inprimatu();
				this.BarajaZatia.remove(x);
				return -13;
			}
			else
			{
				int y=in.nextInt();
				if(x!=-1 && this.getBarajaZatia().get(x).getBaliotasuna()==this.BarajaZatia.get(y).getBaliotasuna() && 
						this.BarajaZatia.get(y).getBaliotasuna()>=momentukoBalioa &&
						x!=y)
				{
					int mahaira_botatakoa=this.getBarajaZatia().get(x).getBaliotasuna();
					System.out.println();
					this.BarajaZatia.get(x).inprimatu();
					this.BarajaZatia.get(y).inprimatu();
					this.BarajaZatia.remove(x);
					this.BarajaZatia.remove(y-1);
					return mahaira_botatakoa;
				}
			}

		}
		else if(zenbanaka==3)
		{
			System.out.println("-1"+"--> paso");
			for(int i=0; i<this.getBarajaZatia().size(); i++)
			{
				System.out.print(i+"--> ");
				this.getBarajaZatia().get(i).inprimatu();
			}

			Scanner in=new Scanner(System.in);
			int x=in.nextInt();
			if(x==-1)return -1;
			else if(this.getBarajaZatia().get(x).getNumero()==2 && this.getBarajaZatia().get(x).getPalo()==1)
			{
				this.BarajaZatia.get(x).inprimatu();
				this.BarajaZatia.remove(x);
				return -13;
			}
			else
			{
				int y=in.nextInt();
				int z=in.nextInt();
				if(x!=-1 && 
						this.getBarajaZatia().get(x).getBaliotasuna()==this.BarajaZatia.get(y).getBaliotasuna()&&
						this.getBarajaZatia().get(x).getBaliotasuna()==this.BarajaZatia.get(z).getBaliotasuna()
						&& this.BarajaZatia.get(y).getBaliotasuna()>=momentukoBalioa&&
						x!=y && x!=z && y!=z)	
				{
					int mahaira_botatakoa=this.getBarajaZatia().get(x).getBaliotasuna();
					System.out.println();
					this.BarajaZatia.get(x).inprimatu();
					this.BarajaZatia.get(y).inprimatu();
					this.BarajaZatia.get(z).inprimatu();
					this.BarajaZatia.remove(x);
					this.BarajaZatia.remove(y-1);
					this.BarajaZatia.remove(z-2);
					return mahaira_botatakoa;
				}
			}

		}
		else if(zenbanaka==4)
		{
			System.out.println("-1"+"--> paso");
			for(int i=0; i<this.getBarajaZatia().size(); i++)
			{
				System.out.print(i+"--> ");
				this.getBarajaZatia().get(i).inprimatu();
			}

			Scanner in=new Scanner(System.in);
			int x=in.nextInt();
			if(x==-1)return -1;
			else if(this.getBarajaZatia().get(x).getNumero()==2 && this.getBarajaZatia().get(x).getPalo()==1)
			{
				this.BarajaZatia.get(x).inprimatu();
				this.BarajaZatia.remove(x);
				return -13;
			}
			else
			{
				int y=in.nextInt();
				int z=in.nextInt();
				int w=in.nextInt();
				if(x!=-1 && 
						this.getBarajaZatia().get(x).getBaliotasuna()==this.BarajaZatia.get(y).getBaliotasuna()&&
						this.getBarajaZatia().get(x).getBaliotasuna()==this.BarajaZatia.get(z).getBaliotasuna()&&
						this.getBarajaZatia().get(x).getBaliotasuna()==this.BarajaZatia.get(w).getBaliotasuna()
						&& this.BarajaZatia.get(y).getBaliotasuna()>=momentukoBalioa&&
						x!=y && x!=z && x!=w && y!=z && y!=w && z!=w)	
				{
					int mahaira_botatakoa=this.getBarajaZatia().get(x).getBaliotasuna();
					System.out.println();
					this.BarajaZatia.get(x).inprimatu();
					this.BarajaZatia.get(y).inprimatu();
					this.BarajaZatia.get(z).inprimatu();
					this.BarajaZatia.get(w).inprimatu();
					this.BarajaZatia.remove(x);
					this.BarajaZatia.remove(y-1);
					this.BarajaZatia.remove(z-2);
					this.BarajaZatia.remove(w-3);
					return mahaira_botatakoa;
				}

			}
		}

		System.out.println();
		System.out.println("paso");
		return -1;


	}


	public String getKlasea() {
		return klasea;
	}


	public void setKlasea(String klasea) {
		this.klasea = klasea;
	}

	private boolean botaBaioEz(int baliotasuna, int zenbanaka, Jokalari J, Karta K, int[] indizeak)
	{

		if(zenbanaka==1)
		{
			if(J.getBarajaZatia().size()==1 )return true;//karta bakarra badu, bota ta bukatudu du.

			if(baliotasuna==1)// mahai gainean 3koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>0)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>0)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>0)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>50)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>55)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>60)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>100)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==2)// mahai gainean 4koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==3)// mahai gainean 5koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==4)// mahai gainean 6koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==5)// mahai gainean 7koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==6)// mahai gainean 10koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==7)// mahai gainean 11koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==8)// mahai gainean 12koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==9)// mahai gainean 12koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==10)// mahai gainean 2koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
		}

		if(zenbanaka==2)
		{
			if(J.getBarajaZatia().size()==2 )return true;//2 karta badu, bota ta bukatu du.

			if((J.getBarajaZatia().get(indizeak[0]).getNumero()==2 && J.getBarajaZatia().get(indizeak[0]).getPalo()==1)//parejako bat 2ko urrea bada ez bota
					||(J.getBarajaZatia().get(indizeak[1]).getNumero()==2 && J.getBarajaZatia().get(indizeak[1]).getPalo()==1))return false;

			if(baliotasuna==1)// mahai gainean 3koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==2)// mahai gainean 4koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==3)// mahai gainean 5koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==4)// mahai gainean 6koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==5)// mahai gainean 7koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==6)// mahai gainean 10koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==7)// mahai gainean 11koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==8)// mahai gainean 12koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==9)// mahai gainean 12koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
			if(baliotasuna==10)// mahai gainean 2koa badago
			{
				if(K.getBaliotasuna()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==2)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==3)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==4)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==5)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==6)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==7)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==8)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==9)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getNumero()==2 && K.getPalo()==1)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
				else if(K.getBaliotasuna()==10)
				{
					if(Jokatu.numeroAleatorio(200)>178)return true;
					else return false;
				}
			}
		}
		if(zenbanaka==3 && K.getBaliotasuna()!=10){
			return true;
		}
		else if(zenbanaka==3 && K.getBaliotasuna()==10)
		{
			if(Jokatu.numeroAleatorio(200)>198)return true;
			else return false;
		}
		else if(zenbanaka==4 && K.getBaliotasuna()<9)return true;
		else if(zenbanaka==4 && K.getBaliotasuna()<10)
		{
			if(Jokatu.numeroAleatorio(200)>178)return true;
			else return false;
		}

		return true;
	}

	@SuppressWarnings("unused")
	private static boolean batBikoUrreaDa(Jokalari J, int[] indizeak)
	{
		for(int i=0; i<indizeak.length; i++)
			if(J.getBarajaZatia().get(indizeak[i]).getNumero()==2 && J.getBarajaZatia().get(indizeak[i]).getPalo()==1)
				return true;
		return false;
	}

	private static int BikoUrreaBadu(Jokalari J)
	{
		for(int i=0; i<J.getBarajaZatia().size(); i++)
			if(J.getBarajaZatia().get(i).getNumero()==2 && J.getBarajaZatia().get(i).getPalo()==1)
				return i;
		return -1;
	}
	private static boolean bikoUrrieBotaBaiOez(int zenbanaka, int baliotasuna)
	{
		return true;
	}
	private static boolean parejaAldu(Karta K, Jokalari J)
	{
		for(int i=0; i<J.getBarajaZatia().size(); i++)
		{
			if(K.getBaliotasuna()==J.getBarajaZatia().get(i).getBaliotasuna())
				if(Jokatu.numeroAleatorio(200)>158)return false;
				else return true;
		}
		return true;
	}
	private static boolean triorikBai(Karta K, Jokalari J)
	{
		int k=0;
		for(int i=0; i<J.getBarajaZatia().size(); i++)
		{
			if(K.getBaliotasuna()==J.getBarajaZatia().get(i).getBaliotasuna())k++;
		}
		if(k>2)
			if(Jokatu.numeroAleatorio(200)>158)return true;
			else return false;
		else return true;
	}
}
